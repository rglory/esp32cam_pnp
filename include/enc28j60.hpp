#ifndef ENC28J60_HPP
#define ENC28J60_HPP

#include <Arduino.h>
#include <SPI.h>
#include <IPAddress.h>
#include <CircularBuffer.h>


namespace ESTATbits {
    enum : byte {
        CLKRDY  = 1 << 0,
        TXABRT  = 1 << 1,
        RXBUSY  = 1 << 2,
        LATECOL = 1 << 4,
        BUFFER  = 1 << 6,
        INT     = 1 << 7
    };
}

namespace ECON2bits {
    enum : byte {
        VRPS    = 1 << 3,
        PWSV    = 1 << 5,
        PKTDEC  = 1 << 6,
        AUTOINC = 1 << 7
    };
}

namespace ECON1bits {
    enum : byte {
        BSEL0    = 1 << 0,
        BSEL1    = 1 << 1,
        RXEN     = 1 << 2,
        TXRTS    = 1 << 3,
        CSUMEN   = 1 << 4,
        DMAST    = 1 << 5,
        RXRST    = 1 << 6,
        TXRST    = 1 << 7
    };
}

namespace ERXFCONbits {
    enum : byte {
        BCEN     = 1 << 0,
        MCEN     = 1 << 1,
        HTEN     = 1 << 2,
        MPEN     = 1 << 3,
        PMEN     = 1 << 4,
        CRCEN    = 1 << 5,
        ANDOR    = 1 << 6,
        UCEN     = 1 << 7
    };
}

namespace EIRbits {
    enum : byte {
        RXERIF   = 1 << 0,
        TXERIF   = 1 << 1,
        TXIF     = 1 << 3,
        LINKIF   = 1 << 4,
        DMAIF    = 1 << 5,
        PKTIF    = 1 << 6
    };
}

namespace MACON1bits {
    enum : byte {
        MARXEN  = 1 << 0,
        PASSALL = 1 << 1,
        RXPAUS  = 1 << 2,
        TXPAUS  = 1 << 3
    };
}

namespace MACON3bits {
    enum : byte {
        FULLDPX = 1 << 0,
        FRMLNEN = 1 << 1,
        HFRMEN  = 1 << 2,
        PHDREN  = 1 << 3,
        TXCRCEN = 1 << 4,
        PADCFG0 = 1 << 5,
        PADCFG1 = 1 << 6,
        PADCFG2 = 1 << 7,
    };
}

namespace MACON4bits {
    enum : byte {
        NOBKOFF = 1 << 4,
        BPEN    = 1 << 5,
        DEFER   = 1 << 6
    };
}


namespace MICMDbits {
    enum : byte {
        MIIRD   = 1 << 0,
        MIISCAN = 1 << 1
    };
}

namespace MISTATbits {
    enum : byte {
        BUSY    = 1 << 0,
        SCAN    = 1 << 1,
        INVALID = 1 << 2
    };
}

namespace PHCON1bits {
    enum : uint16_t {
        PDPXMD   = 1 << 8,
        PPWRSW   = 1 << 11,
        PLOOPBK  = 1 << 14,
        PRST     = 1 << 15
    };
}
namespace PHCON2bits {
    enum : uint16_t {
        HDLDIS   = 1 << 8,
        JABBER   = 1 << 10,
        TXDIS    = 1 << 13,
        FRCLNK   = 1 << 14
    };
}

struct EthernetHeader {
    byte     dstMac[6];
    byte     srcMac[6];
    uint16_t tag;
}__attribute__((packed));

struct ARPPacket {
    uint16_t htype;
    uint16_t ptype;
    byte     hrdwAddLen;
    byte     protoLen;
    uint16_t oper;
    byte     senderMac[6];
    uint32_t senderIP;
    byte     targetMac[6];
    uint32_t targetIP;
}__attribute__((packed));

struct IPHeader {
    byte     version_ihl   = 0x45;
    byte     dscp_ecn      = 0;
    uint16_t length        = 0;
    uint16_t ident         = 0;
    uint16_t flags_offset  = 0;
    byte     ttl           = 64;
    byte     proto         = 17;    // UDP
    uint16_t chksum        = 0;
    uint32_t senderIP      = 0;
    uint32_t targetIP      = 0;

    void computeChksum() 
    {
        uint32_t sum = 0;
        const auto e = reinterpret_cast<uint16_t *>(this + 1);
        for( auto p = reinterpret_cast<uint16_t *>(this); p != e; ++p )
            sum += *p;
        while( sum>>16 )
            sum = (sum & 0xFFFF) + (sum >> 16);
        chksum = ~sum;
    }
}__attribute__((packed));

struct ICMPHeader {
    byte     type;
    byte     code;
    uint16_t chcksum;
    uint32_t hdata;
    void computeChksum( const byte *data, uint16_t len )
    {
        uint32_t sum = 0;
        const auto e = reinterpret_cast<uint16_t *>(this + 1);
        const uint16_t *p;
        for( p = reinterpret_cast<const uint16_t *>(this); p != e; ++p )
            sum += *p;
        for( p = reinterpret_cast<const uint16_t *>(data); len >= 2; len -= 2 )
            sum += *p++;
        if( len )
            sum += *reinterpret_cast<const byte *>( p );

        while( sum>>16 )
            sum = (sum & 0xFFFF) + (sum >> 16);

        chcksum = ~sum;
    }
}__attribute__((packed));

struct UDPHeader {
    uint16_t sport         = 0;
    uint16_t dport         = 0;
    uint16_t length        = 0;
    uint16_t chksum        = 0;
}__attribute__((packed));
 
class ENC28J60 {
public:
    static const uint16_t maxPacket        = 1518;
    static const uint16_t maxUDPPacket     = maxPacket - sizeof( EthernetHeader ) - sizeof( IPHeader ) - sizeof( UDPHeader ) - 4;
    static const uint16_t readMemoryBegin  = 0;
    static const uint16_t readMemoryEnd    = 2048; // 2K for read buffer
    static const uint16_t writeMemoryBegin = readMemoryEnd;
    static const uint16_t writeMemoryEnd   = 8192;

    struct RecvHeader {
        uint16_t next;
        uint16_t size;
        byte     status[2];
    };

    enum Opcode : byte {
        ocRCR = 0b00000000,
        ocRBM = 0b00111010,
        ocWCR = 0b01000000,
        ocWBM = 0b01111010,
        ocBFS = 0b10000000,
        ocBFC = 0b10100000,
        ocSRC = 0b11111111
    };

    enum CommonRegs : byte {
        rEIE   = 0x1B,
        rEIR   = 0x1C,
        rESTAT = 0x1D,
        rECON2 = 0x1E,
        rECON1 = 0x1F
    };

    enum RegsBank0 : byte {
        rERDPTL   = 0x00,
        rERDPTH   = 0x01,
        rEWRPTL   = 0x02,
        rEWRPTH   = 0x03,
        rETXSTL   = 0x04,
        rETXSTH   = 0x05,
        rETXNDL   = 0x06,
        rETXNDH   = 0x07,
        rERXSTL   = 0x08,
        rERXSTH   = 0x09,
        rERXNDL   = 0x0A,
        rERXNDH   = 0x0B,
        rERXRDPTL = 0x0C,
        rERXRDPTH = 0x0D,
        rERXWRPTL = 0x0E,
        rERXWRPTH = 0x0F,
        rEDMASTL  = 0x10,
        rEDMASTH  = 0x11,
        rEDMANDL  = 0x12,
        rEDMANDH  = 0x13,
        rEDMADSTL = 0x14,
        rEDMADSTH = 0x15,
        rEDMACSL  = 0x16,
        rEDMACSH  = 0x17,
    };

    enum RegsBank1 : byte {
        rEHT0     = 0x00,
        rEHT1     = 0x01,
        rEHT2     = 0x02,
        rEHT3     = 0x03,
        rEHT4     = 0x04,
        rEHT5     = 0x05,
        rEHT6     = 0x06,
        rEHT7     = 0x07,
        rEPMM0    = 0x08,
        rEPMM1    = 0x09,
        rEPMM2    = 0x0A,
        rEPMM3    = 0x0B,
        rEPMM4    = 0x0C,
        rEPMM5    = 0x0D,
        rEPMM6    = 0x0E,
        rEPMM7    = 0x0F,
        rEPMCSL   = 0x10,
        rEPMCSH   = 0x11,
        rEPMOL    = 0x14,
        rEPMOH    = 0x15,
        rERXFCON  = 0x18,
        rEPKTCNT  = 0x19,
    };

    enum RegsBank2 : byte {
        rMACON1   = 0x00,
        rMACON3   = 0x02,
        rMACON4   = 0x03,
        rMABBIPG  = 0x04,
        rMAIPGL   = 0x06,
        rMAIPGH   = 0x07,
        rMACLCON1 = 0x08,
        rMACLCON2 = 0x09,
        rMAMXFLL  = 0x0A,
        rMAMXFLH  = 0x0B,
        rMICMD    = 0x12,
        rMIREGADR = 0x14,
        rMIWRL    = 0x16,
        rMIWRH    = 0x17,
        rMIRDL    = 0x18,
        rMIRDH    = 0x19,
    };

    enum RegsBank3 : byte {
        rMAADR5   = 0x00,
        rMAADR6   = 0x01,
        rMAADR3   = 0x02,
        rMAADR4   = 0x03,
        rMAADR1   = 0x04,
        rMAADR2   = 0x05,
        rEBSTSD   = 0x06,
        rEBSTCON  = 0x07,
        rEBSTCSL  = 0x08,
        rEBSTCSH  = 0x09,
        rMISTAT   = 0x0A,
        rEREVID   = 0x12,
        rECOCON   = 0x15,
        rEFLOCON  = 0x17,
        rEPAUSL   = 0x18,
        rEPAUSH   = 0x19,
    };

    enum RegsPHY {
        rPHCON1   = 0x00,
        rPHSTAT1  = 0x01,
        rPHID1    = 0x02,
        rPHID2    = 0x03,
        rPHCON2   = 0x10,
        rPHSTAT2  = 0x11,
        rPHIE     = 0x12,
        rPHIR     = 0x13,
        rPHLCON   = 0x14
    };

    template<typename R>
    byte readReg( R r );

    template<typename R>
    void writeReg( R r, byte data );

    template<typename R>
    void setReg( R r, byte mask );

    template<typename R>
    void resetReg( R r, byte mask );

    template<typename R>
    void writeReg16( R r, uint16_t data )
    {
        writeReg( r, data );
        writeReg( static_cast<R>(r + 1), data >> 8 );
    }

    template<typename R>
    uint16_t readReg16( R r )
    {
        return readReg( r ) | ( static_cast<uint16_t>( readReg( static_cast<R>(r + 1) ) ) << 8 );
    }

    ENC28J60( const byte *macaddr, IPAddress addr, byte cs ) :
        m_spis( 8000000, SPI_MSBFIRST, SPI_MODE0 ),
        m_addr( addr ),
        m_macaddress( macaddr ),
        m_cs( cs )
    {
    }

    using UdpCallback = void(*)( const EthernetHeader *e, const IPHeader *ip, const UDPHeader *udp, byte *data, byte len );

    void addCallback( uint16_t port, UdpCallback cb );

    // processing incoming and outgoing buffers
    void process();   

    // returns true if there is enough room in buffer
    bool pushUDP( const byte *mac, uint32_t ip, uint16_t sport, uint16_t dport,
                  byte bdata,
                  const byte *data, uint16_t len  );


    uint16_t read( void *data, uint16_t len );
    uint16_t write( byte b );
    uint16_t write16( uint16_t data );
    uint16_t write32( uint32_t data );
    uint16_t writeZeros( uint16_t len );
    uint16_t write( const void *data, uint16_t len );

    byte begin(); // initializes ENC28J60 returns erevid on success, 0 otherwise

    IPAddress addr() const { return m_addr; }

    friend class CSHandler;
private:
    struct CallbackData {
        uint16_t    m_port;
        UdpCallback m_cb;
    };

    struct SendBlock {
        uint16_t m_start;
        uint16_t m_end;
    };

    static const int maxCallbacks = 4;

    void softReset_();
    bool prepareWrite_( uint16_t size );
    byte readReg_( byte r, bool dummy );
    void writeReg_( byte r, byte data );
    void setReg_( byte r, byte mask );
    void resetReg_( byte r, byte mask );

    void checkSwitchBank_( byte bank );

    CallbackData                m_callbacks[maxCallbacks];
    CircularBuffer<SendBlock,8> m_sendBlocks;
    SPISettings                 m_spis;
    IPAddress                   m_addr;
    const byte                 *m_macaddress;
    uint16_t                    m_nextRead      = 0;
    byte                        m_callbackCount = 0;
    byte                        m_cs            = 0;
    byte                        m_bank          = 0;
};

template<>
inline
byte ENC28J60::readReg<ENC28J60::CommonRegs>( CommonRegs r )
{
    return readReg_( r, false );
}

template<>
inline
void ENC28J60::writeReg<ENC28J60::CommonRegs>( CommonRegs r, byte value )
{
    writeReg_( r, value);
}

template<>
inline
void ENC28J60::setReg<ENC28J60::CommonRegs>( CommonRegs r, byte mask )
{
    setReg_( r, mask );
}

template<>
inline
void ENC28J60::resetReg<ENC28J60::CommonRegs>( CommonRegs r, byte mask )
{
    resetReg_( r, mask );
}

template<>
inline
byte ENC28J60::readReg<ENC28J60::RegsBank0>( RegsBank0 r )
{
    checkSwitchBank_( 0 );
    return readReg_( r, false );
}

template<>
inline
void ENC28J60::writeReg<ENC28J60::RegsBank0>( RegsBank0 r, byte value )
{
    checkSwitchBank_( 0 );
    writeReg_( r, value);
}

template<>
inline
byte ENC28J60::readReg<ENC28J60::RegsBank1>( RegsBank1 r )
{
    checkSwitchBank_( 1 );
    return readReg_( r, false );
}

template<>
inline
void ENC28J60::writeReg<ENC28J60::RegsBank1>( RegsBank1 r, byte value )
{
    checkSwitchBank_( 1 );
    writeReg_( r, value);
}

template<>
inline
byte ENC28J60::readReg<ENC28J60::RegsBank2>( RegsBank2 r )
{
    checkSwitchBank_( 2 );
    return readReg_( r, true );
}

template<>
inline
void ENC28J60::writeReg<ENC28J60::RegsBank2>( RegsBank2 r, byte value )
{
    checkSwitchBank_( 2 );
    writeReg_( r, value);
}

template<>
inline
byte ENC28J60::readReg<ENC28J60::RegsBank3>( RegsBank3 r )
{
    checkSwitchBank_( 3 );
    return readReg_( static_cast<byte>( r ), r < RegsBank3::rEBSTSD or r == RegsBank3::rMISTAT );
}

template<>
inline
void ENC28J60::writeReg<ENC28J60::RegsBank3>( RegsBank3 r, byte value )
{
    checkSwitchBank_( 3 );
    writeReg_( r, value);
}

template<>
inline
uint16_t ENC28J60::readReg16<ENC28J60::RegsPHY>( RegsPHY r )
{
    writeReg( rMIREGADR, r );
    setReg_( rMICMD, MICMDbits::MIIRD );
    delayMicroseconds( 11 );
    while( readReg( rMISTAT ) & MISTATbits::BUSY ); // poll till not busy
    resetReg_( rMICMD, MICMDbits::MIIRD );
    return readReg16( rMIRDL );
}


template<>
inline
void ENC28J60::writeReg16<ENC28J60::RegsPHY>( RegsPHY r, uint16_t value )
{
    writeReg( rMIREGADR, r );
    writeReg16( rMIWRL, value );
    delayMicroseconds( 11 );
    while( readReg( rMISTAT ) & MISTATbits::BUSY ); // poll till not busy
}


#endif // ENC28J60_HPP