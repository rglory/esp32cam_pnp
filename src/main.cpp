#include <Arduino.h>
#include <enc28j60.hpp>
#include <esp_camera.h>
#include <lwip/def.h>


const byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };        // ethernet interface mac address
const uint16_t udpPort = 5000;                                 // UDP port to listen
ENC28J60 enc( mymac, IPAddress( 192, 168,101,10), 15 );        // our IP and SPI CS Pin

bool initCamera();

void handleUdpRequest( const EthernetHeader *e, 
                       const IPHeader *ip, 
                       const UDPHeader *udp, 
                       byte *data, 
                       byte len  );

void setup() {
    Serial.begin( 115200 );

    Serial.print( "Total PSRAM:" ); Serial.println( ESP.getPsramSize() );

    auto revid = enc.begin();
    if( not revid ) {
        Serial.println( "ENC28J60 initialization failed" );
        delay( 1000 );
        esp_restart();
    }
    Serial.printf( "Initialized ENC28J60 with revid %u\n", revid );
    initCamera();

    enc.addCallback( udpPort, handleUdpRequest );
}

byte         theirMac[6];
uint32_t     theirIP = 0;
uint16_t     theirPort = 0;

camera_fb_t *pic;
byte        *picDataPtr = 0;
char         sequence = 0;

void handleUdpRequest( const EthernetHeader *e, 
                       const IPHeader *ip, 
                       const UDPHeader *udp, 
                       byte *data, 
                       byte len )
{
    if( len == 0 ) return;
    while( len and ( data[len-1] == '\n' or data[len-1] == '\r' ) )
        --len;
    data[len] = 0;

    Serial.print( "got request from " );
    Serial.print( IPAddress( ip->senderIP ) );
    Serial.print( ":" );
    Serial.print( ntohs( udp->sport ) );
    Serial.print( " - " );
    Serial.println( reinterpret_cast<const char *>( data ) );

    char resp[64];
    byte respLength = 0;
    switch( data[0] ) {
        case 'S' : // STOP
            picDataPtr = nullptr; 
            if( pic )
                esp_camera_fb_return( pic );
            theirIP = 0; 
            respLength = sprintf( resp, "0 stop ok" );
            break; 
        case 's' : // START
            if( len < 3 or len > 10 or data[1] != ' ' ) {
                respLength = sprintf( resp, "2 invalid start command" );
                break;
            }
            theirPort  = atoi( reinterpret_cast<const char *>( data ) + 2 );
            if( theirPort == 0 ) {
                respLength = sprintf( resp, "3 invalid port on start command %s", data + 2 );
                break;
            }
            picDataPtr = nullptr; 
            memcpy( theirMac, e->srcMac, 6 );
            theirIP = ip->senderIP;
            respLength = sprintf( resp, "0 streaming port %d", theirPort );
            break;
        case 'L' : // LED off
            respLength = sprintf( resp, "0 led off ok" );
            break;
        default:
            respLength = sprintf( resp, "1 invalid command %c", data[0] );
    }
    if( respLength ) {
        enc.pushUDP( e->srcMac, ip->senderIP, udpPort, ntohs( udp->sport ), ' ', reinterpret_cast<const byte *>( resp ), respLength );
        Serial.print( "\t" );
        Serial.println( resp );
    }
}


void loop() {
    static auto timer = millis();

    enc.process();
    if( not picDataPtr and theirIP and millis() - timer > 2 ) {
        pic = esp_camera_fb_get();
        //Serial.printf( "capturing frame %d format %d\n", pic->len, pic->format );
        picDataPtr = pic->buf;
        sequence = 0;
    }

    if( picDataPtr )  {
        bool lastPacket = true;
        auto len = pic->len - (picDataPtr - pic->buf);
        if( len >= ENC28J60::maxUDPPacket ) {
            len = ENC28J60::maxUDPPacket - 1;
            lastPacket = false;
        }
        if( enc.pushUDP( theirMac, theirIP, udpPort, theirPort, sequence * ( lastPacket ? -1 : 1 ), picDataPtr, len ) ) {
            ++sequence;
            picDataPtr = lastPacket ? nullptr : picDataPtr + len;
            if( lastPacket ) {
                esp_camera_fb_return( pic );
                timer = millis();
            }
        } else
            delay( 1 );
    }

}   
 