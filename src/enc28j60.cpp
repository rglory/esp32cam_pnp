#include <enc28j60.hpp>
#include <SPI.h>
#include <lwip/def.h>

struct CSHandler {
    ENC28J60 *m_enc;

    CSHandler( ENC28J60 *enc ) : m_enc( enc ) 
    { 
        SPI.beginTransaction( m_enc->m_spis );
        digitalWrite( m_enc->m_cs, LOW ); 
    }

    ~CSHandler() 
    { 
        digitalWrite( m_enc->m_cs, HIGH ); 
        SPI.endTransaction();
    }

    CSHandler(const CSHandler &) = delete;
    CSHandler operator=(const CSHandler &) = delete;
};

byte ENC28J60::begin()
{
    SPI = SPIClass( HSPI );
    SPI.setFrequency( m_spis._clock );
    SPI.begin();
    //SPI.setHwCs( true );
    
    pinMode( m_cs, OUTPUT );

    // errata p.19 clr PWESV befor reset
    resetReg( rECON2, ECON2bits::PWSV);
    delay( 1 );

    softReset_();

    auto start = millis();
    while( ( readReg( rESTAT ) & ESTATbits::CLKRDY ) == 0 ) {
        if( millis() - start > 500 ) {
            Serial.println( "Timeout on ENC28J60 CLKRDY initialization" );
            return 0; // timeout
        }
        delay( 10 );
    }

    writeReg_( rECON1, 0 );
    m_bank = 0;
    // initialize read buffer
    writeReg16( rERXSTL,   readMemoryBegin );
    writeReg16( rERXRDPTL, readMemoryEnd - 1 );
    writeReg16( rERXNDL,   readMemoryEnd - 1 ); // rERXND is inclusive

    writeReg( rMACON1, MACON1bits::MARXEN | MACON1bits::TXPAUS | MACON1bits::RXPAUS ); 

    writeReg( rMACON3, MACON3bits::PADCFG0 | MACON3bits::TXCRCEN ); 

    const byte maadrRegs[] = { rMAADR6, rMAADR5, rMAADR4,rMAADR3,rMAADR2,rMAADR1 };
    // macaddress
    for( int i = 0; i < 6; ++i )
        writeReg( static_cast<decltype(rMAADR6)>( maadrRegs[i] ), m_macaddress[i] );

    writeReg( rMACON4, MACON4bits::DEFER );

    writeReg( rMABBIPG, 0x12 ); // recommended for half duplex

    writeReg( rMAIPGL, 0x12 ); // recommended by datasheet

    writeReg( rMAIPGH, 0x0C ); // recommended by datasheet for half duplex

    writeReg16( rMAMXFLL, maxPacket ); // set maximum packet size

    writeReg16( rPHCON1, 0 );                   // set half duplex
    writeReg16( rPHCON2, PHCON2bits::HDLDIS );  // disable loopback

    writeReg( rERXFCON, ERXFCONbits::CRCEN );  // check CRC

    setReg_( rECON1, ECON1bits::RXEN ); // enable reception

    return readReg( rEREVID );
}

uint16_t ENC28J60::read( void *data, uint16_t len )
{
    CSHandler h( this );
    SPI.write( ocRBM );
    SPI.transferBytes( nullptr, static_cast<byte *>(data ), len );
    return len;
}

uint16_t ENC28J60::write( byte b )
{
    CSHandler h( this );
    SPI.write( ocWBM );
    SPI.write( b );
    return sizeof(b);
}

uint16_t ENC28J60::write16( uint16_t data )
{
    CSHandler h( this );
    SPI.write( ocWBM );
    SPI.writeBytes( reinterpret_cast<byte *>(&data), sizeof(data) );
    return sizeof( data );
}

uint16_t ENC28J60::write32( uint32_t data )
{
    CSHandler h( this );
    SPI.write( ocWBM );
    SPI.writeBytes( reinterpret_cast<byte *>(&data), sizeof(data) );
    return sizeof( data );
}

uint16_t ENC28J60::writeZeros( uint16_t len )
{
    byte z = 0;
    CSHandler h( this );
    SPI.write( ocWBM );
    SPI.writePattern( &z, 1, len );
    return len;
}

uint16_t ENC28J60::write( const void *data, uint16_t len )
{
    CSHandler h( this );
    SPI.write( ocWBM );
    SPI.transferBytes( static_cast<const byte *>( data ), nullptr, len );
    return len;
}

byte ENC28J60::readReg_( byte r, bool dummy )
{
    CSHandler h( this );
    SPI.write( ocRCR | r );
    if( dummy ) SPI.transfer( 0 );
    return SPI.transfer( 0 );
}

void ENC28J60::writeReg_( byte r, byte data )
{
    CSHandler h( this );
    SPI.write( ocWCR | r );
    SPI.write( data );
}

void ENC28J60::setReg_( byte r, byte mask )
{
    CSHandler h( this );
    SPI.write( ocBFS | r );
    SPI.write( mask );
}

void ENC28J60::resetReg_( byte r, byte mask )
{
    CSHandler h( this );
    SPI.write( ocBFC | r );
    SPI.write( mask );
}

void ENC28J60::softReset_()
{
    CSHandler h( this );
    SPI.write( ocSRC );

}

void ENC28J60::checkSwitchBank_( byte bank )
{
    byte d = m_bank ^ bank;
    if( d == 0 ) return;

    byte toSet = d & bank;
    if( toSet ) setReg_( rECON1, toSet );

    byte toReset = d & m_bank;
    if( toReset ) resetReg_( rECON1, toReset );

    m_bank = bank;
}

void ENC28J60::addCallback( uint16_t port, UdpCallback cb )
{
    if( m_callbackCount < maxCallbacks )
        m_callbacks[m_callbackCount++] = CallbackData{ port, cb };
}

void printMac( const byte *mac )
{
    for( int i = 0; i < 6; ++i ) {
        if( i ) Serial.print( ':' );
        Serial.printf( "%02X", mac[i] );
    }
}

void ENC28J60::process()   // returns how much room is available for UDP
{
    if( readReg( ENC28J60::rEPKTCNT ) ) {

        ENC28J60::RecvHeader enchdr;
        writeReg16( ENC28J60::rERDPTL, m_nextRead );
        read( &enchdr, sizeof( enchdr ) );
        m_nextRead = enchdr.next;

        do {
            EthernetHeader ethdr;
            read( &ethdr, sizeof(ethdr) );
            if( ethdr.tag == 0x0608 ) { // ARP in big endian
                ARPPacket arppacket;
                read( &arppacket, sizeof(arppacket) );
                if( arppacket.oper == 0x0100 and IPAddress(arppacket.targetIP) == m_addr ) {
                    if( prepareWrite_( sizeof( ethdr ) + sizeof( arppacket ) + 1 ) ) {
                        memcpy( ethdr.dstMac, ethdr.srcMac, 6 );
                        memcpy( ethdr.srcMac, m_macaddress, 6 );

                        arppacket.oper = 0x0200;
                        memcpy( arppacket.targetMac, arppacket.senderMac, 6 );
                        memcpy( arppacket.senderMac, m_macaddress, 6 );
                        arppacket.targetIP = arppacket.senderIP;
                        arppacket.senderIP = m_addr;

                        {
                            CSHandler h( this );

                            SPI.write( ocWBM );
                            SPI.write( 0 );
                            SPI.writeBytes( reinterpret_cast<byte *>( &ethdr ), sizeof( ethdr ) );
                            SPI.writeBytes( reinterpret_cast<byte *>( &arppacket ), sizeof( arppacket ) );
                        }
                    }
                }
                break;
            }
            if( ethdr.tag == 0x0008 ) { // IP in big endian
                IPHeader ip;
                {
                    CSHandler h( this );
                    SPI.write( ocRBM );
                    SPI.transferBytes( nullptr, reinterpret_cast<byte *>( &ip ), sizeof(ip) );
                }

                if( ip.targetIP != m_addr ) // not to us
                    break;

                if( ip.proto == 1 ) { // ICMP
                    ICMPHeader icmp;
                    auto len = ntohs( ip.length ) - sizeof( icmp ) - sizeof( ip );
                    byte data[len]; // prohibited in c++, oh well...
                    {
                        CSHandler h( this );
                        SPI.write( ocRBM );
                        SPI.transferBytes( nullptr, reinterpret_cast<byte *>( &icmp ), sizeof(icmp) );
                        if( icmp.type != 8 ) // we respond only to ping request
                            break;
                        if( len )
                            SPI.transferBytes( nullptr, data, len );
                    }

                    if( not prepareWrite_( sizeof( ethdr ) + sizeof( ip ) + sizeof( icmp ) + len + 1 ) ) 
                        break;

                    memcpy( ethdr.dstMac, ethdr.srcMac, 6 );
                    memcpy( ethdr.srcMac, m_macaddress, 6 );

                    ip.targetIP = ip.senderIP;
                    ip.senderIP = m_addr;
                    ip.chksum   = 0;
                    ip.computeChksum();

                    icmp.type = 0;
                    icmp.chcksum = 0;
                    icmp.computeChksum( data, len );

                    {
                        CSHandler h( this );

                        SPI.write( ocWBM );
                        SPI.write( 0 );
                        SPI.writeBytes( reinterpret_cast<byte *>( &ethdr ), sizeof( ethdr ) );
                        SPI.writeBytes( reinterpret_cast<byte *>( &ip ), sizeof( ip ) );
                        SPI.writeBytes( reinterpret_cast<byte *>( &icmp ), sizeof( icmp ) );
                        if( len )
                            SPI.writeBytes( data, len );
                    }
                    break;
                }

                if( ip.proto == 17 ) { // UDP
                    UDPHeader udp;
                    byte data[128];
                    uint16_t len = 0;
                    {
                        CSHandler h( this );
                        SPI.write( ocRBM );
                        
                        SPI.transferBytes( nullptr, reinterpret_cast<byte *>( &udp ), sizeof(udp) );
                        len = ntohs( udp.length ) - sizeof( udp );
                        if( len >= sizeof(data) ) break;
                        SPI.transferBytes( nullptr, data, len );
                    }
                    auto port = ntohs( udp.dport );
                    for( int i = 0; i < m_callbackCount; ++i ) {
                        if( m_callbacks[i].m_port == port ) m_callbacks[i].m_cb( &ethdr, &ip, &udp, data, len );
                    }
                }
            } 
        } while( 0 );
        writeReg16( ENC28J60::rERXRDPTL, m_nextRead == 0 ? readMemoryEnd - 1 : m_nextRead - 1 );
        setReg( ENC28J60::rECON2, ECON2bits::PKTDEC );
    }
    if( ( readReg( ENC28J60::rECON1 ) & ECON1bits::TXRTS ) == 0 and m_sendBlocks.size() ) {
        auto block = m_sendBlocks.shift();
        writeReg16( ENC28J60::rETXSTL, block.m_start );
        writeReg16( ENC28J60::rETXNDL, block.m_end - 1 );
        setReg( ENC28J60::rECON1, ECON1bits::TXRTS );
    }
}

bool ENC28J60::prepareWrite_( uint16_t size )
{
    if( m_sendBlocks.size() == m_sendBlocks.capacity ) 
        return false;

    SendBlock block { writeMemoryBegin, static_cast<uint16_t>( writeMemoryBegin + size ) };

    if( m_sendBlocks.size() ) {
        block.m_start = m_sendBlocks.last().m_end + 7;
        block.m_start += block.m_start % 2;
        if( block.m_start + size + 7 > writeMemoryEnd  )
            block.m_start = writeMemoryBegin;

        block.m_end = block.m_start + size;
        if( block.m_end + 7 > m_sendBlocks.first().m_start ) 
            return false;
    }
    writeReg16( ENC28J60::rEWRPTL, block.m_start );

    m_sendBlocks.push( std::move( block ) );
    return true;
}


bool ENC28J60::pushUDP( const byte *mac, uint32_t ipaddr, uint16_t sport, uint16_t dport,
                        byte bdata,
                        const byte *data, uint16_t len  )
{
    auto size = len + 1 + sizeof(IPHeader) + sizeof(UDPHeader) + sizeof( EthernetHeader );
    if( not prepareWrite_( size + 1 ) ) {
        return false;
    }

    size -= sizeof( EthernetHeader );
    // IP Header
    IPHeader ip;
    ip.length   = htons( size );
    ip.senderIP = m_addr;
    ip.targetIP = ipaddr;
    ip.computeChksum();

    size -= sizeof(IPHeader);

    // UDP Header
    UDPHeader udp;
    udp.sport  = htons( sport );
    udp.dport  = htons( dport );
    udp.length = htons( size );

    {
        CSHandler h( this );
        SPI.write( ocWBM );
        SPI.write(0);
        SPI.writeBytes( mac, 6 );
        SPI.writeBytes( m_macaddress, 6 );
        SPI.write16( 0x0800 ); // IPV4 big endian, SPI converts to big endian

        SPI.writeBytes( reinterpret_cast<byte *>( &ip ), sizeof(ip) );
        SPI.writeBytes( reinterpret_cast<byte *>( &udp ), sizeof(udp) );
        SPI.write( bdata );
        SPI.writeBytes( data, len );
    }

    return true;
}
